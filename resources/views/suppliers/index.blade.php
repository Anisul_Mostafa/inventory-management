 
@extends('layouts.app')

@section('title')

Add Supplier 
@endsection

@section("page")


<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                  
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
    
                            <button class="btn btn-info "><a href="{{route('suppliers.create')}}">Create Supplier</a></button>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                <table class="table table-striped table-hover js-basic-example dataTable">
                                <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th >Name</th>
                                                <th>Shop Name</th>
                                                <th >Phone</th>
                                                <th >Email </th>
                                                <th>Addrss</th>
                                                <th>Account Holder</th>
                                                <th>Account Number</th>
                                                <th>Bank Name</th>
                                                <th>Branch Name</th>
                                                <th  style="float:left;text-align:center;">Action</th>
                                            </tr>
                                        </thead>
              <tbody>
                  @foreach($suppliers as $supplier)
                  <tr>
                      
                      <td> {{$supplier->id}}</td>
                      <td> {{$supplier->name}} </td>
                      <td> {{$supplier->shop}} </td>
                      <td> {{$supplier->phone}} </td>
                      <td> {{$supplier->email}} </td>
                      <td> {{$supplier->address}} </td>
                      <td> {{$supplier->accountholder}} </td>
                      <td> {{$supplier->accountnumber}} </td>
                      <td> {{$supplier->bankname}} </td>
                      <td> {{$supplier->branchname}} </td>
                      <td style="float:left;width:145px;">
                         <form action="{{route('suppliers.destroy',$supplier->id)}}"method="POST">
                      <a class="btn btn-primary btn-sm" href="{{route('suppliers.show',$supplier->id)}}">                  
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="{{route('suppliers.edit',$supplier->id)}}">
              
                              Edit
                          </a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>  
                          </a>
                      </td>
                  </tr>
                 @endforeach
              </tbody>
          </table>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            @endsection