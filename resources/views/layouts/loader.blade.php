<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="{{asset('temp/assets/images/brand/icon_black.svg')}}" width="48" height="48" alt="ArrOw"></div>
        <p>Please wait...</p>
    </div>
</div>