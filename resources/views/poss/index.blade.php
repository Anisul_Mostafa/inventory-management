 
@extends('layouts.app')

@section('title')

POS (Point of Sale)
@endsection

@section("page")


<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                   
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">POS</button>
                        <a title="Date" class="btn btn-success ml-2">{{date('d/m/y')}}</a>
                    </form>
                </div>
            </nav>
                          
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            @php 
                            if($message = Session::get('success'))
                            echo"<div class='alert alert-success alert-dismissible'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                <h5><i class=icon fas fa-check></i> $message </h5>  
                            </div>";
                            @endphp
                       
                            <div class="header">
                            <!-- <h2><a href="{{url('customer.create')}}">Create Customer</a></h2> -->
                            <!-- <button class="btn btn-info "><a href="{{route('products.create')}}">Create Product </a></button> -->
                            </div>
                            <div class="body" style="float:left;clearfix;">
                             <div class="row" style="float:left;clearfix;">
                                    <div class="col-lg-8" style="float:left;clearfix; border-right:1px solid gray;">
                                             
                                            <h4 class="text-info">Customer</h4>
                                            <div style="width:714px;">
                                            <a href="#" class="btn btn btn-primary" style="float:left;clearfix;" data-toggle="modal" data-target="#modal_5">Add New</a>&nbsp;
                                                @php
                                                $customers =DB::table('customers')->get();
                                                @endphp
                                             
                                                <select name="name" class="col-lg-9 form-control show-tick ms select2 text-black"  >
                                                    <!-- <option disabled="" selected="" style="font-color:red;">Select Customer</option> -->
                                                       <option selected="" style="font-color:red;">Select Customer</option>
                                                    @foreach($customers as $customer)
                                                    <option value="{{$customer->id}}">{{$customer->name}}</option>
                                                    @endforeach
                                                </select><br><hr>
                                                </div>

                                                <div class="row clearfix" >
                                            
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="card pricing2">
                                                        <div class="body" style="background-color:white;">
                                                            <div class="pricing-plan" >
                                                                <h3> Add Product for invoice </h3>
                                                                <ul class="pricing-features" style="border:1px solid blue">
                                                                <table class="table table-striped table-hover">
                                                                    <thead class="bg-info">
                                                                    <tr>
                                                                        <th class="text-white">Name</th>
                                                                        <th class="text-white">Quantity</th>
                                                                        <th class="text-white">Single&nbsp;Price</th>
                                                                        <th class="text-white">Sub&nbsp;Total</th>
                                                                        <th class="text-white">Action</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Laptop Battery 102</td>
                                                                            <form>
                                                                                    <td><input type="number" name="" value="1" style="width:40px;">&nbsp;
                                                                                    <button type="submit" class="btn btn-sm btn-success" style="width:40px;height: 37px;" >&nbsp;
                                                                                        <i class="fa fa-check"></i>
                                                                                    </button>
                                                                                </from>
                                                                            </td>
                                                                            <td>3000</td>
                                                                            <td>4552</td>
                                                                            <td><i class="fa fa-trash-o" style="color:red;font-size:24px;"></i></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </ul>
                                                                <div class="col-12" style="background-color:black;">
                                                                    <h3>POS Pricing</h3>
                                                                    <hr>
                                                                    <p style="font-size:18px;">Quantity:&nbsp;000</p>
                                                                    <p style="font-size:18px;">vat:&nbsp; 0 </p>
                                                                    <hr>
                                                                    <p><h3 class="text-white">Total:&nbsp;</h3><h3 class="text-white">1522 TAKA</h3></p>
                                                            
                                                                </div>
                                                                <button type="submit" class="btn btn-outline-primary">Create Invoice</button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                <div class="col-lg-4" style="float:left;clearfix;">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover js-basic-example dataTable">
                                            <thead>
                                                <tr>
                                                <th>Cart</th>
                                                    <th>ID</th>
                                                    <th data-breakpoints="xs">Product&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp; </th>
                                                    <th data-breakpoints="xs">Product&nbsp;Code </th>
                                                    <th data-breakpoints="xs">Buying&nbsp;Price </th>
                                                    <th data-breakpoints="xs">Selling&nbsp;Price</th>
                                                    <th data-breakpoints="xs" style="float:right;text-align:center;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($products as $product)
                                                    <tr>
                                                        <td>
                                                        <i class="fa fa-shopping-cart" style="font-size:22px; color:#00AFFB"></i>
                                                        </td>
                                                        <td> 
                                                    
                                                        {{$product->id }} 
                                                    
                                                    </td>
                                                        <td> {{$product->name}} </td>
                                                        <td> {{$product->code}} </td>
                                                        <td> {{$product->buying_price}} </td>
                                                        <td> {{$product->selling_price}} </td>
                                                        <td style="float:left;width:155px;">
                                                            <form action="{{route('products.destroy',$product->id)}}"method="POST">
                                                            <a class="btn btn-info btn-sm" href="{{route('products.edit',$product->id)}}">
                                                
                                                                Edit
                                                            </a>
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                                </form>  
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                    </div>
                                </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
      <!-- Modals -->

                    <div class="row-wrapper">
                        <div class="row cols-xs-space cols-sm-space cols-md-space">
                           
                            <div class="col-lg-3 col-md-6">
                                <!-- Button trigger modal -->
                            
                                <div class="modal modal-tertiary fade" id="modal_5" tabindex="-1" role="dialog" aria-labelledby="modal_5" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modal_title_6">Add a New Customer</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="py-3 text-left">
                                                <form id="basic-form" method="POST" action="{{route('customers.store')}}" >
                                                    @csrf
                                                    <div class="form-group">
                                                        <label> Name   </label>
                                                        <input type="text" name="name" class="form-control" placehoder="customers Name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone Number </label>
                                                        <input type="phone" name="phone" class="form-control" placehoder="customers Phone" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email </label>
                                                        <input type="email" name="email" class="form-control" placehoder="customers Email" required>
                                                    </div>
                                                

                                                    <div class="form-group">
                                                        <label>Address </label>
                                                        <textarea type="text" name="address" class="form-control" placehoder="customers Address" required></textarea>
                                                    </div>
                                                    
                                            
                                                
                                                    <button type="submit" class="btn btn-primary" style="text-align:center; content:center;">Add Customer </button>
                                                </form>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endsection