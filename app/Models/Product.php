<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'sup_id','cat_id','uni_id','code', 'qty','buy_date','expire_date','description', 'buying_price','selling_price',
];
}
