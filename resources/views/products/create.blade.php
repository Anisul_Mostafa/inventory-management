
@extends('layouts.app')

@section('title')

create Product
@endsection

@section("page")

<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="#" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                                <!-- <h2><a href="{{url('customer.index')}}">Manage Customer</a></h2> -->
                                <button class="btn btn-info "><a href="{{route('products.index')}}">Manage Attendance</a></button>
                            </div>
                            <div class="body">
                            <div class="col-md-12">
                        <div class="card">
                          
                            <div class="body">
                                <form id="basic-form" method="POST" action="{{route('products.store')}}" >
                                    @csrf
                                    <div class="form-group">
                                        <label> Name   </label>
                                        <input type="text" name="name" class="form-control" placehoder="Product Name" required> 
                                       
                                    </div>
                                    <div class="form-group">
                                        <label>Supplier Name </label>
                                        @php
                                            $suppliers =DB::table('suppliers')->get();
                                        @endphp
                                        <select name="sup_id"  class="form-control" style="color:#FB00A4 ;">
                                            @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}">{{$supplier->shop}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Category Name </label>
                                        @php
                                            $catagories =DB::table('catagories')->get();
                                        @endphp
                                        <select name="cat_id"  class="form-control" style="color:#00AFFB;">
                                            @foreach($catagories as $catagorie)
                                            <option value="{{$catagorie->id}}">{{$catagorie->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Unit </label>
                                        @php
                                            $units =DB::table('units')->get();
                                        @endphp
                                        <select name="uni_id"  class="form-control" style="color:#A0FB00;">
                                            @foreach($units as $unit)
                                            <option value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Code  </label>
                                        <input type="text" name="code" class="form-control" placehoder="Product Code" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Quantity </label>
                                        <input type="number" name="qty" class="form-control" placehoder="Product Quantity" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Buying Date  </label>
                                        <input type="date" name="buy_date" class="form-control" placehoder="Buying Date" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Expire Date </label>
                                        <input type="date" name="expire_date" class="form-control" placehoder="Expire Date" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea type="text" name="description" class="form-control" placehoder="Description" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Buying Price</label>
                                        <input type="text" name="buying_price" class="form-control" placehoder="Buying Price" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Selling Price</label>
                                        <input type="text" name="selling_price" class="form-control" placehoder="Selling Price" required>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add Product</button>
                                </form>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               
            @endsection