


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="ThemeMakker">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <title>@yield('login')</title>
    <link rel="stylesheet" href="{{asset('temp/assets/vendor/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('temp/assets/vendor/fontawesome/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('temp/assets/css/main.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('temp/assets/css/dark.css')}}" type="text/css">
</head>

<body class="theme-black full-dark">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img src="{{asset('temp/assets/images/brand/icon_black.svg')}}" width="48" height="48" alt="ArrOw"></div>
            <p>Please wait...</p>
        </div>
    </div>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
                    <div class="top">
                        <img src="{{asset('temp/assets/images/brand/icon.svg')}}" alt="Lucid">
                        <strong>Inventory Management System</strong> <span></span>
                    </div>
					<div class="card">
                        <div>
                            @if(session('status'))
                            <div class="alert alert-danger" style="text-align:center">
                            {{session('status')}}
                            </div>
                            @endif
                            @if(session('bye'))
                            <div class="alert alert-info" style="text-align:center">
                            {{session('bye')}}
                            </div>
                            @endif
                        </div>
                        <div class="header">
                            <p class="lead">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login to your account</p>
                        </div>
                        <div class="body">
                            <form class="form-auth-small" action="{{url('login')}}" method="post">
                            @csrf
                            <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only" autofilout="false">Username</label>
                                    <input type="text" class="form-control" id="signin-email" value="{{session('username')?session('username'):''}}" name="txtUsername" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" id="signin-password" value="{{session('password')?session('password'):''}}" name="txtPassword" placeholder="Password">
                                </div>
                                <div class="form-group clearfix">
                                    <label class="fancy-checkbox element-left">
                                        <input type="checkbox">
                                        <span>Remember me</span>
                                    </label>								
                                </div>
                                <!-- <a href="{{url('/layouts.dashboard')}}"> -->
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Sign In">
                                <!-- <button  type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                         -->
                            <!-- </a> -->
                                <div class="bottom">
                                    <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="auth-forgot-password.html">Forgot password?</a></span>
                                    <span>Don't have an account? <a href="auth-register.html">Register</a></span>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
    <!-- END WRAPPER -->
    
<!-- Core -->
<script src="{{asset('temp/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('temp/assets/bundles/vendorscripts.bundle.js')}}"></script>

<script src="{{asset('temp/assets/js/theme.js')}}"></script>
</body>
</html>

