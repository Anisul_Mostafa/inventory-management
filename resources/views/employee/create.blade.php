
@extends('layouts.app')

@section('title')

Add Employee
@endsection

@section("page")

<link href="https://cdnjs.cloudflare.com/ajax/libs/toaster.js/2.0.1/css/toastr.css" rel="stylesheet"/>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Users</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Timeline</a>
                            <a class="dropdown-item" href="javascript:void(0);">Invoices</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Stater page</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Pricing</a>
                            <a class="dropdown-item" href="javascript:void(0);">Search</a>
                            <a class="dropdown-item" href="javascript:void(0);">Testimonials</a>
                            <a class="dropdown-item" href="javascript:void(0);">Map</a>
                            <a class="dropdown-item" href="javascript:void(0);">Icon</a>
                            <a class="dropdown-item" href="javascript:void(0);">Carousel</a>
                            <a class="dropdown-item" href="javascript:void(0);">Gallery</a>
                            <a class="dropdown-item" href="javascript:void(0);">Lookup</a>
                        </div>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                            <button class="btn btn-info "><a href="{{url('employee.index')}}">Manage Employee</a></button>
                            </div>
                            <div class="body">
                            <div class="col-md-12">
                        <div class="card">
                          
                            <div class="body">
                                <form id="basic-form" method="POST" action="{{url('employee.store')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label> Name   </label>
                                        <input type="text" name="name" class="form-control" placehoder="Employee Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" placehoder="Employee Email" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" name="phone" class="form-control" placehoder="Employee Phone Number" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Address </label>
                                        <input type="text" name="address" class="form-control" placehoder="Employee Address" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Experience</label>
                                        <input type="text" name="experience" class="form-control" placehoder="Employee Experience" required>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label>Salary</label>
                                        <input type="text" name="salary" class="form-control" placehoder="Employee Salary" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Vacation</label>
                                        <input type="text" name="vacation" class="form-control" placehoder="Employee Vacation" required>
                                    </div>
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" name="city" class="form-control" placehoder="Employee City" required>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label>Text Area</label>
                                        <textarea class="form-control" rows="5" cols="30" required></textarea>
                                    </div> -->
                                    <div class="form-group">
                                        
                                        <label>Photo</label>
                                        <img id="image" src="#">
                                        <input type="file" name="photo" class="form-control" placehoder="Employee Photo"
                                        accept="image/*" class="upload"
                                        required onchange="readURL(this)">
                                    
                                    </div>
                                    <button type="submit" class="btn btn-primary">Add Employee</button>
                                </form>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <script>
                    function readURL(input){
                        if(input.files && input.files[0]){
                            let reader = new FileReader();
                            reader.onload = function(e){
                                $('#image')
                                    .attr('src',e.target.result)
                                    .width(80)
                                    .height(80);
                            };
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>
                <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/toaster.js/2.0.1/js/toastr.js" ></script>
<script>
                @if(Session::has('messege'))
                    var type="{{Session::get('alert-type','info')}}"
                    switch(type){
                        case 'info':
                        toastr.info("{{Session::get('messege')}}");
                        break;
                        case 'success':
                        toastr.success("{{Session::get('messege')}}");
                        break;
                        case 'warning':
                        toastr.warning("{{Session::get('messege')}}");
                        break;
                        case 'error':
                        toastr.error("{{Session::get('messege')}}");
                        break;
                    }
                    @endif

</script>
            @endsection