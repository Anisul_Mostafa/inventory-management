 
@extends('layouts.app')

@section('title')

Attendance 
@endsection

@section("page")


<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                  
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                            <!-- <h2><a href="{{url('customer.create')}}">Create Customer</a></h2> -->
                            <button class="btn btn-info "><a href="{{route('attendances.create')}}">Create Attendance</a></button>
                            </div>
                            <div class="body">
                                @if($message = Session::get('success'))
                                
                            <div class='alert alert-success alert-dismissible'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                <h5><i class=icon fas fa-check></i>{{ $message }}</h5>  
                            </div>
                                   
                                </div>
                                @endif 
                                <div class="table-responsive">
                                <table class="table table-striped table-hover js-basic-example dataTable">
                                    
                                <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th data-breakpoints="xs">Name</th>
                                                <th data-breakpoints="xs">In Time </th>
                                                <th data-breakpoints="xs">Out Time </th>
                                                <th data-breakpoints="xs">Date </th>
                                                <th data-breakpoints="xs" style="float:right;text-align:center;">Action</th>
                                            </tr>
                                        </thead>
              <tbody>
                  @foreach($attendances as $attendance)
                  <tr>
                      
                      <td> {{$attendance->id }} </td>
                      <td> {{$attendance->name}} </td>
                      <td> {{$attendance->in_time}} </td>
                      <td> {{$attendance->out_time}} </td>
                      <td> {{$attendance->date}} </td>
                 
                      <td class="project-actions text-right">
                         <form action="{{route('attendances.destroy',$attendance->id)}}"method="POST">
                      <a class="btn btn-primary btn-sm" href="{{route('attendances.show',$attendance->id)}}">                  
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="{{route('attendances.edit',$attendance->id)}}">
              
                              Edit
                          </a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>  
                          </a>
                      </td>
                  </tr>
                 @endforeach
              </tbody>
          </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            @endsection