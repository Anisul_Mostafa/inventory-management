 
@extends('layouts.app')

@section('title')

Products 
@endsection

@section("page")


<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                  
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                            <!-- <h2><a href="{{url('customer.create')}}">Create Customer</a></h2> -->
                            <button class="btn btn-info "><a href="{{route('products.create')}}">Create Product </a></button>
                            </div>
                            <div class="body">
                                @if($message = Session::get('success'))
                                
                            <div class='alert alert-success alert-dismissible'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                <h5><i class=icon fas fa-check></i>{{ $message }}</h5>  
                            </div>
                                   
                                </div>
                                @endif 
                                <div class="table-responsive">
                                <table class="table table-striped table-hover js-basic-example dataTable">
                                    
                                <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th data-breakpoints="xs">Product&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp; </th>
                                                <th data-breakpoints="xs">Supplier&nbsp;Name </th>
                                                <th data-breakpoints="xs">Category </th>
                                                <th data-breakpoints="xs">Unit</th>
                                                <th data-breakpoints="xs">Product&nbsp;Code </th>
                                                <th data-breakpoints="xs">Quantity</th>
                                                <th data-breakpoints="xs">Buying&nbsp;Date </th>
                                                <th data-breakpoints="xs">Expire&nbsp;Date </th>
                                                <th data-breakpoints="xs">Description </th>
                                                <th data-breakpoints="xs">Buying&nbsp;Price </th>
                                                <th data-breakpoints="xs">Selling&nbsp;Price</th>
                                                <th data-breakpoints="xs" style="float:right;text-align:center;">Action</th>
                                            </tr>
                                        </thead>
              <tbody>
                  @foreach($products as $product)
                  <tr>
                      
                      <td> {{$product->id }} </td>
                      <td> {{$product->name}} </td>
                      <td> {{$product->sup_id}} </td>
                      <td> {{$product->cat_id}} </td>
                      <td> {{$product->uni_id}} </td>
                      <td> {{$product->code}} </td>
                      <td> {{$product->qty}} </td>
                      <td> {{$product->buy_date}} </td>
                      <td> {{$product->expire_date}} </td>
                      <td> {{$product->description}} </td>
                      <td> {{$product->buying_price}} </td>
                      <td> {{$product->selling_price}} </td>
                      <td style="float:left;width:155px;">
                         <form action="{{route('products.destroy',$product->id)}}"method="POST">
                      <a class="btn btn-primary btn-sm" href="{{route('products.show',$product->id)}}">                  
                              View
                          </a>
                          <a class="btn btn-info btn-sm" href="{{route('products.edit',$product->id)}}">
              
                              Edit
                          </a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>  
                          </a>
                      </td>
                  </tr>
                 @endforeach
              </tbody>
          </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            @endsection