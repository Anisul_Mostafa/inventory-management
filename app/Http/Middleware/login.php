<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user_id=session('sess_id');
        if(!isset($user_id)){
            return redirect("/")->with('status','Please Login First');
        }
        return $next($request);
    }
}
