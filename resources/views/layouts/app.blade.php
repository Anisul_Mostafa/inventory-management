<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Webpixels">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<title>@yield('title')</title>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toaster.js/2.0.1/css/toastr.css" rel="stylesheet"/>

<link rel="stylesheet" href="{{asset('temp/assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('temp/assets/vendor/themify-icons/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('temp/assets/vendor/fontawesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('temp/assets/vendor/fullcalendar/fullcalendar.min.css')}}">
<link rel="stylesheet" href="{{asset('temp/assets/vendor/charts-c3/plugin.css')}}"/>
<link rel="stylesheet" href="{{asset('temp/assets/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}"/>
<link rel="stylesheet" href="{{asset('temp/assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('temp/assets/css/dark.css')}}">

<link rel="stylesheet" href="{{asset('temp/assets/vendor/select2/select2.css')}}" />

</head>
<body class="theme-black full-dark">

{{--@include('layouts.test')--}}


@include('layouts.loader')
@include('layouts.nav')


<div class="main_content" id="main-content">
@include('layouts.leftsidebar')
@include('layouts.rightsidebar')
@include('layouts.mainpage')
</div>

<!-- Core -->
<script src="{{asset('temp/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('temp/assets/bundles/vendorscripts.bundle.js')}}"></script>

<script src="{{asset('temp/assets/bundles/c3.bundle.js')}}"></script>
<script src="{{asset('temp/assets/bundles/jvectormap.bundle.js')}}"></script> <!-- JVectorMap Plugin Js -->
<script src="{{asset('temp/assets/js/pages/calendar.js')}}"></script>
<script src="{{asset('temp/assets/bundles/fullcalendarscripts.bundle.js')}}"></script>
<script src="{{asset('temp/assets/js/theme.js')}}"></script>
<script src="{{asset('temp/assets/js/pages/index.js')}}"></script>
<script src="{{asset('temp/assets/js/pages/todo-js.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toaster.js/2.0.1/js/toastr.js" ></script>
<script src="{{asset('temp/assets/js/pages/widgets.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-knob/jquery.knob.min.js')}}"></script> 
<script src="{{asset('temp/assets/js/pages/tables/jquery-datatable.js')}}"></script>
<script src="{{asset('temp/assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-datatable/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('temp/assets/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>



<!-- selec Box -->
<script src="{{asset('temp/assets/vendor/select2/select2.min.js')}}"></script> <!-- Select2 Js -->

<script src="{{asset('temp/assets/js/pages/advanced-form.js')}}"></script>


<script>
                @if(Session::has('messege'))
                    var type="{{Session::get('alert-type','info')}}"
                    switch(type){
                        case 'info':
                        toastr.info("{{Session::get('messege')}}");
                        break;
                        case 'success':
                        toastr.success("{{Session::get('messege')}}");
                        break;
                        case 'warning':
                        toastr.warning("{{Session::get('messege')}}");
                        break;
                        case 'error':
                        toastr.error("{{Session::get('messege')}}");
                        break;
                    }
                    @endif

</script>
</body>
</html>