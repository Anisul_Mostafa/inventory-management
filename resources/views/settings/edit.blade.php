
@extends('layouts.app')

@section('title')

Edit Setting
@endsection

@section("page")

<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                   
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                                <h2><a href="{{route('settings.index')}}">Manage Attendance</a></h2>
                            </div>
                            <div class="body">
                            <div class="col-md-12">
                        <div class="card">
                          
                            <div class="body">
                                <form id="basic-form" method="POST" action="{{ route('settings.update',$setting->id) }}" >
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label> Name   </label>
                                        <input type="text" name="name" value="{{$setting->name}}" class="form-control" placehoder="setting Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone </label>
                                        <input type="text" name="phone" value="{{$setting->phone}}" class="form-control" placehoder="setting phone" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Address </label>
                                        <input type="text" name="address" value="{{$setting->address}}" class="form-control" placehoder="setting Address" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email </label>
                                        <input type="email" name="email" value="{{$setting->email}}" class="form-control" placehoder="setting email" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Logo   </label>
                                        <input type="text" name="logo" value="{{$setting->logo}}" class="form-control" placehoder="setting Logo" required>
                                    </div>
                                    <div class="form-group">
                                        <label> City   </label>
                                        <input type="text" name="city" value="{{$setting->city}}" class="form-control" placehoder="setting City" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Country   </label>
                                        <input type="text" name="country" value="{{$setting->country}}" class="form-control" placehoder="setting Country" required>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Update Settings</button>
                                </form>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               
            @endsection