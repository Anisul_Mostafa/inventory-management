<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
 use DB;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=DB::table('products')
        ->join('suppliers','products.sup_id','suppliers.id')
        ->join('catagories','products.cat_id','catagories.id')
        ->join('units','products.uni_id','units.id')
        ->select('products.*','catagories.name','suppliers.shop','units.name')
        ->orderBy('id')
        ->get();
       return view('products.index',compact('products'));
    //    php artisan make:controller PosController --resource --model=Pos
    //    php artisan make:migration create_poss_table --create=poss
        // $products = Product::latest()->paginate(20);
        // return view('products.index',compact('products'))
        // ->with('i', (request()->input('page',1) - 1)*20);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'sup_id' => 'required',
            'cat_id' => 'required',
            'uni_id' => 'required',
            'code' => 'required',
            'qty' => 'required',
            'buy_date' => 'required',
            'expire_date' => 'required',
            'description' => 'required',
            'buying_price' => 'required',
            'selling_price' => 'required',
        ]);
        Product::create($request->all());
        return redirect()->route('products.index')
        ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([

        ]);
        $product->update($request->all());
        return redirect()->route('products.index')
        ->with('success','Product Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')
        ->with('success','Product deleted successfully');
    }
}
