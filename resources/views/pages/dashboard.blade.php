
@extends('layouts.app')

@section('title')

welcome page 
@endsection

@section("page")


<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">@yield('content')</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">   
                <li class="nav-item dropdown">
                        <a class="nav-link" href="{{url('/dashboard')}}" role="button" >Dashboard</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{route('blank')}}" role="button" >Contact</a>
                    </li>                     
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Users</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Timeline</a>
                            <a class="dropdown-item" href="javascript:void(0);">Invoices</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Stater page</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Pricing</a>
                            <a class="dropdown-item" href="javascript:void(0);">Search</a>
                            <a class="dropdown-item" href="javascript:void(0);">Testimonials</a>
                            <a class="dropdown-item" href="javascript:void(0);">Map</a>
                            <a class="dropdown-item" href="javascript:void(0);">Icon</a>
                            <a class="dropdown-item" href="javascript:void(0);">Carousel</a>
                            <a class="dropdown-item" href="javascript:void(0);">Gallery</a>
                            <a class="dropdown-item" href="javascript:void(0);">Lookup</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <button type="button" class="btn btn-primary">Add</button>
                    <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                </form>
            </div>
        </nav>
        <div class="container-fluid">
        <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                        <div class="card tasks_report">
                            <div class="body">
                                <input type="text" class="knob" value="66" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#26dad2" readonly>                        
                                <h6 class="m-t-20">Satisfaction Rate</h6>
                                <p class="displayblock m-b-0">47% Average <i class="fa fa-trending-up"></i></p>                        
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                        <div class="card tasks_report">
                            <div class="body">
                                <input type="text" class="knob" value="26" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#7b69ec" readonly>
                                <h6 class="m-t-20">Project Panding</h6>
                                <p class="displayblock m-b-0">13% Average <i class="fa fa-trending-down"></i></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                        <div class="card tasks_report">
                            <div class="body">
                                <input type="text" class="knob" value="76" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#f9bd53" readonly>
                                <h6 class="m-t-20">Productivity Goal</h6>
                                <p class="displayblock m-b-0">75% Average <i class="fa fa-trending-up"></i></p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                        <div class="card tasks_report">
                            <div class="body">
                                <input type="text" class="knob" value="88" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#00adef" readonly>
                                <h6 class="m-t-20">Total Revenue</h6>
                                <p class="displayblock m-b-0">54% Average <i class="fa fa-trending-up"></i></p>
                            </div>
                        </div>
                    </div>            
                </div>
            <!-- <div class="row clearfix">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <h6>Traffic</h6>
                            <h2>20 <small class="info">of 1Tb</small></h2>
                            <small>2% higher than last month</small>
                            <div class="progress mb-0">
                                <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon sales">
                        <div class="body">
                            <h6>Sales</h6>
                            <h2>12% <small class="info">of 100</small></h2>
                            <small>6% higher than last month</small>
                            <div class="progress mb-0">
                                <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100" style="width: 38%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon email">
                        <div class="body">
                            <h6>Email</h6>
                            <h2>39 <small class="info">of 100</small></h2>
                            <small>Total Registered email</small>
                            <div class="progress mb-0">
                                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100" style="width: 39%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon domains">
                        <div class="body">
                            <h6>Domains</h6>
                            <h2>8 <small class="info">of 10</small></h2>
                            <small>Total Registered Domain</small>
                            <div class="progress mb-0">
                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row clearfix">
            <div class="col-lg-12 col-md-12 d-flex">
                        <div class="card flex-fill">
                            <div class="body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-12 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Revenue Statistics</h2>
                        </div>
                        <div class="body">
                            <div class="d-flex bd-highlight mb-4">
                                <div class="flex-fill bd-highlight">
                                    <h5 class="mb-0">21,521 <i class="fa fa-angle-up"></i></h5>
                                    <small>Today</small>
                                </div>
                                <div class="flex-fill bd-highlight">
                                    <h5 class="mb-0">%12.35 <i class="fa fa-angle-down"></i></h5>
                                    <small>Last month %</small>
                                </div>
                            </div>
                            <div id="chart-bar-rotated" class="c3_chart"></div>
                        </div>
                    </div>
                </div>
               
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="header">
                            <h2>ToDo List</h2>
                        </div>
                        <div class="body todo_list">
                            <div class="form-group d-flex mb-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Type your task here...">
                                </div>
                                <button class="btn btn-primary ml-2" type="button" id="button-addon2">Add</button>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Walk the dog this evening
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Go shopping at 3 PM
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Keep coding 'till you're dead
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Enjoy every moment you have
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Sleep well tonight
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Sleep well tonight
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
            <div class="col-lg-12 col-md-12 d-flex">
                        <div class="card flex-fill">
                            <div class="body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="row clearfix">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Visitors Statistics</h2>
                        </div>
                        <div class="body">
                            <div id="world-map-markers" class="jvector-map" style="height: 405px;"></div>
                        </div>
                    </div>
                </div>
                
            </div>
          
            
        </div>
        @endsection