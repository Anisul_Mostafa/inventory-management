 
@extends('layouts.app')

@section('title')

Edit Supplier
@endsection

@section("page")

<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                   
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="https://themeforest.net/user/wrraptheme/portfolio" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                                <h2><a href="{{route('suppliers.index')}}">Manage Attendance</a></h2>
                            </div>
                            <div class="body">
                            <div class="col-md-12">
                        <div class="card">
                          
                            <div class="body">
                                <form id="basic-form" method="POST" action="{{ route('suppliers.update',$supplier->id) }}" >
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label> Name   </label>
                                        <input type="text" name="name" value="{{$supplier->name}}" class="form-control" placehoder="supplier Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Compnay Name   </label>
                                        <input type="text" name="shop" value="{{$supplier->shop}}" class="form-control" placehoder="supplier Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone </label>
                                        <input type="text" name="phone" value="{{$supplier->phone}}" class="form-control" placehoder="supplier phone" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email </label>
                                        <input type="email" name="email" value="{{$supplier->email}}" class="form-control" placehoder="supplier email" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Address </label>
                                        <input type="text" name="address" value="{{$supplier->address}}" class="form-control" placehoder="supplier Address" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Account Holder   </label>
                                        <input type="text" name="accountholder" value="{{$supplier->accountholder}}" class="form-control" placehoder="Account Holder" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Accoutn Number   </label>
                                        <input type="text" name="accountnumber" value="{{$supplier->accountnumber}}" class="form-control" placehoder="supplier Account Number" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Bank Name   </label>
                                        <input type="text" name="bankname" value="{{$supplier->bankname}}" class="form-control" placehoder="Bank Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Branch Name   </label>
                                        <input type="text" name="branchname" value="{{$supplier->branchname}}" class="form-control" placehoder="Branch Name" required>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update Customer</button>
                                </form>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               
            @endsection