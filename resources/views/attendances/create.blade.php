
@extends('layouts.app')

@section('title')

create Attendance
@endsection

@section("page")

<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="#" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                                <!-- <h2><a href="{{url('customer.index')}}">Manage Customer</a></h2> -->
                                <button class="btn btn-info "><a href="{{route('attendances.index')}}">Manage Attendance</a></button>
                            </div>
                            <div class="body">
                            <div class="col-md-12">
                        <div class="card">
                          
                            <div class="body">
                                <form id="basic-form" method="POST" action="{{route('attendances.store')}}" >
                                    @csrf
                                    <div class="form-group">
                                        <label> Name   </label>
                                        <!-- <input type="text" name="name" class="form-control" placehoder="Attendance Name" required> -->
                                        @php
                                            $employees =DB::table('employees')->get();
                                        @endphp
                                        <select name="name"  class="form-control" style="color:Black;">
                                            @foreach($employees as $employee)
                                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>In Time </label>
                                        <input type="time" name="in_time" class="form-control" placehoder="In Time" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Out Time </label>
                                        <input type="time" name="out_time" class="form-control" placehoder="Out Time" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Date </label>
                                        <input type="date" name="date" class="form-control" placehoder="Attendance Date" required>
                                    </div>
                                    
                               
                                 
                                    <button type="submit" class="btn btn-primary">Add Attendance</button>
                                </form>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               
            @endsection