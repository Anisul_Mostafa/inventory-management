<div class="left_sidebar">
        <nav class="sidebar">
            <div class="user-info">
                <div class="image"><a href="javascript:void(0);"><img src="{{asset('temp/assets/images/me.jpg')}}" alt="User" style="width:50px"></a></div>
                <div class="detail mt-3">
                    <h6 class="mb-0">
                    @if(session('sess_user_name'))
                        {{session('sess_user_name')}}
                        <br>
                        {{session('sess_role_id')}}

                    @endif

                    </h6>
                   
                </div>
                <div class="social">
                    <a href="javascript:void(0);" title="facebook"><i class="ti-twitter-alt"></i></a>
                    <a href="javascript:void(0);" title="twitter"><i class="ti-linkedin"></i></a>
                    <a href="javascript:void(0);" title="instagram"><i class="ti-facebook"></i></a>
                </div>
            </div>
            <ul id="main-menu" class="metismenu">
                <li class="g_heading">Main</li>
                <li class="active"><a href="{{url('/dashboard')}}"><i class="ti-home"></i><span>Dashboard</span></a></li>
                <!-- <li class=""><a href="{{url('/students')}}"><i class="ti-user"></i><span>Manage Student</span></a></li> -->
                <li><a href="{{route('poss.index')}}"><i class="ti-vector"></i><span>POS</span></a></li>
                <!-- <li><a href="#"><i class="ti-vector"></i><span>Purchase</span></a></li> -->
                <li class="g_heading">Reports</li>
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-view-list"></i><span>Reports</span></a>
                    <ul>
                        <li><a href="#">Sales Report</a></li>
                        <li><a href="#">Purchase Report</a></li>
                    </ul>
                </li>
                <li class="g_heading">Products Section</li>
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-pencil-alt"></i><span>Products</span></a>
                    <ul>
                        <li><a href="{{route('products.index')}}">Manage Product </a></li>
                        <li><a href="{{route('catagorys.index')}}">Manage Catagory</a></li>
                        <!-- <li><a href="#">Manage Sub-Catagory</a></li> -->
                        <li><a href="{{route('units.index')}}">Manage Unit</a></li>
                        <!-- <li><a href="#">Manage Size</a></li>
                        <li><a href="#">Manage Brand</a></li> -->
                    </ul>
                </li>
                <!-- <li class="g_heading">Customer Section</li> -->
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-view-list"></i><span>Customers</span></a>
                    <ul>
                        <li><a href="{{route('customers.index')}}">Manage Customer</a></li>
                        
                    </ul>
                </li>
                
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-view-list"></i><span>Suppliers</span></a>
                    <ul>
                        <li><a href="{{route('suppliers.index')}}">Manage Suppliers</a></li>
                        
                    </ul>
                </li>
                
                <li class="g_heading">Collection Section </li>
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-pencil-alt"></i><span>Collections </span></a>
                    <ul>
                        <li><a href="#">Due Collection</a></li>
                        <li><a href="#">Supplier Due Payment</a></li>
                        <li><a href="#">Income</a></li>
                        <li><a href="#">Expense</a></li>
                        
                    </ul>
                </li>
             
                <li class="g_heading">Authority Sections</li>
              
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-pencil-alt"></i><span>System</span></a>
                    <ul>
                        <li><a href="{{url('user.index')}}">Manage User</a></li>
                        <li><a href="{{route('settings.index')}}">Company Settings</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-pencil-alt"></i><span>HR </span></a>
                    <ul>
                        <!-- <li><a href="{{url(md5('employees'))}}">Add Employee</a></li> -->
                        <li><a href="{{url('employee.index')}}">Manage Employee</a></li>
                        <!-- <li><a href="{{url('all-employee')}}">All Employee</a></li> -->
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-pencil-alt"></i><span>Attendance</span></a>
                    <ul>
                        <li><a href="{{route('attendances.index')}}">Manage Attendance</a></li>
             
                      
                    </ul>
                </li>
   
                <!-- <li class="g_heading">Authentication</li> -->
                <li class="open-top">
                    <a href="javascript:void(0)" class="has-arrow"><i class="ti-lock"></i><span>Authentication</span></a>
                    <ul>
                    
                        <li><a class="dropdown-item" href="auth-forgot-password.html">Forgot password</a></li>
                        <li><a class="dropdown-item" href="auth-lock-screen.html">Lock Screen</a></li>
                        <li><a class="dropdown-item" href="{{url('/logout')}}">Log out</a></li>
                    </ul>
                </li>
            </ul>            
        </nav>
    </div>
