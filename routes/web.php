<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\CatagoryController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PosController;
use App\Http\Middleware\login;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//user controller start
Route::get("user.index",[UserController::class,"index"]);
Route::get("user.create",[UserController::class,"create"]);
Route::post("user.store",[UserController::class,"store"]);
 Route::resource('users',UserController::class);

//user controller end
Route::post("login",[LoginController::class,"login"]);
Route::get("logout",[LoginController::class,"logout"]);


Route::get('/dashboard', function () {

    return view('pages.dashboard');
})->middleware('secured');



Route::get(md5('blank'), function () {
    return view('pages.blank');
})->name('blank');

Route::get('/', function () {
    return view('layouts.login');
});

//employee route
// Route::get('add-employee','EmployeeController@index')->name('add.employee');

// Route::get(md5("add-employee"),[EmployeeController::class,"index"]);

//Route::get("add-employee",[EmployeeController::class,"index"]);

// Route::post("employee.insert-employee",[EmployeeController::class,"store"]);
// Route::get("employee.all-employee",[EmployeeController::class,"Employees"]);
// Route::resource(md5("employee.add-employee"),EmployeeController::class)->middleware('secured');

// Route::resource("employee.add-employee",EmployeeController::class)->middleware('secured');
Route::get("employee.index",[EmployeeController::class,"index"]);
Route::get("employee.create",[EmployeeController::class,"create"]);
Route::post("employee.store",[EmployeeController::class,"store"]);
 Route::resource('employees',EmployeeController::class);


 //Route::resource('attendances','App\Http\Controllers\AttendanceController');
  Route::resource('attendances',AttendanceController::class)->middleware('secured');
  Route::resource('customers',CustomerController::class)->middleware('secured');
  Route::resource('catagorys',CatagoryController::class)->middleware('secured');
  Route::resource('units',UnitController::class)->middleware('secured');
  Route::resource('settings',SettingController::class)->middleware('secured');
  Route::resource('suppliers',SupplierController::class)->middleware('secured');
  Route::resource('products',ProductController::class)->middleware('secured');
  Route::resource('poss',PosController::class)->middleware('secured');
 //Route::resource('customers','App\Http\Controllers\CustomerController');

