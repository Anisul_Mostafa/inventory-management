
@extends('layouts.app')

@section('title')

Add Supplier
@endsection

@section("page")

<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{{url('/dashboard')}}">Dashboard</a>
                <a class="navbar-brand" href="{{url('/blank')}}">Contacts</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-align-justify"></i>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0);">Inbox</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0);">Calendar</a>
                            <a class="dropdown-item" href="javascript:void(0);">TaskBoard</a>
                            <a class="dropdown-item" href="javascript:void(0);">Chat App</a>
                            <a class="dropdown-item" href="javascript:void(0);">Contacts</a>
                        </div>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <button type="button" class="btn btn-primary">Add</button>
                        <a href="#" title="Portfolio" class="btn btn-success ml-2">Portfolio</a>
                    </form>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card planned_task">
                            <div class="header">
                                <!-- <h2><a href="{{url('customer.index')}}">Manage Customer</a></h2> -->
                                <button class="btn btn-info "><a href="{{route('suppliers.index')}}">Manage Customer</a></button>
                            </div>
                            <div class="body">
                            <div class="col-md-12">
                        <div class="card">
                          
                            <div class="body">
                                <form id="basic-form" method="POST" action="{{route('suppliers.store')}}" >
                                    @csrf
                                    <div class="form-group">
                                        <label> Name   </label>
                                        <input type="text" name="name" class="form-control" placehoder="suppliers Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Company Name   </label>
                                        <input type="text" name="shop" class="form-control" placehoder="suppliers Company Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number </label>
                                        <input type="phone" name="phone" class="form-control" placehoder="suppliers Phone" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email </label>
                                        <input type="email" name="email" class="form-control" placehoder="suppliers Email" required>
                                    </div>
                                
                                    <div class="form-group">
                                        <label>Supplier Address </label>
                                        <textarea type="text" name="address" class="form-control" placehoder="suppliers Address" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label> Account Holder   </label>
                                        <input type="text" name="accountholder" class="form-control" placehoder="suppliers Account Holder" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Account Number   </label>
                                        <input type="text" name="accountnumber" class="form-control" placehoder="suppliers Account Number" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Bank Name   </label>
                                        <input type="text" name="bankname" class="form-control" placehoder="suppliers Bank Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label> Branchname   </label>
                                        <input type="text" name="branchname" class="form-control" placehoder="Branch Name" required>
                                    </div>
                                    
                               
                                 
                                    <button type="submit" class="btn btn-primary">Add Customer </button>
                                </form>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               
            @endsection