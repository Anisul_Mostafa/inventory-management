<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
//  use App\Employee;

use Illuminate\Http\Request;
class EmployeeController extends Controller
{
    //    /**
    //  * create controller
    //  * 
    //  * @return void
    //  */
    
    // public function __construct(){
    //     $this->middleware('login');
    // }
    function index(){
         //  return view("all_employee",["employees"=>$employees]);
         try {           
            //DB::connection()->getPdo();            
          $employees=DB::select("select * from employees");        
          return view("employee.index",["employees"=>$employees]);
  
          }catch (\Exception $e) {
              die("Error: ".$e->getMessage());
          }
    }
    public function create()
    {
        return view("employee.create");
    }
    function store(Request $request){
        // $validatedData = $request->validate([
        //     'name' => 'required|max:255',
        //     'email' => 'required|unique:employees|max:255',
        //     'phone' => 'required|max:255',
        //     'address' => 'required|max:255',
        //     'photo' => 'required|max:255',
        //     'salary' => 'required|max:255',
        //     'vacation' => 'required|max:255',
        //     'city' => 'required|max:255',
           
        // ]);
        $data=array();
        
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['phone']=$request->phone;
        $data['address']=$request->address;
        $data['experience']=$request->experience;
        $data['salary']=$request->salary;
        $data['vacation']=$request->vacation;
        $data['city']=$request->city;
        $image = $request->file('photo');
        if($image){
            $image_name= $request->email;
            $ext =strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='public/employee/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if($success){
                $data['photo']=$image_url;
                $employee=DB::table('employees')->insert($data);
                if($employee){
                    $notification=array(
                        'messege'=>'Successfully Employee Inserted',
                        'alert-type'=>'success'
                    );
                    // return Redirect()->route('/')->with($notification);
                    //echo $notification;
                    // echo"seuccesfully added";
                     return view('employee.create');
                }else{
                    $notification=array(
                        'messege'=>'Error',
                        'alert-type'=>'success'
                    );
                    // return Redirect()->back()->with($notification);
                    echo $notification;
                }
            }else{
                // return Redirect()->back();
                echo"first";
            }
        }else{
            echo"second";

        //    echo "<pre>";
    //    print_r($data);
    //    exit();
        
    }
    }

    public function Employees(){
        // $employees=Employee::all();
        // $employees=DB::table('employees')->get();

// echo "ok";
       
        //  $employees=DB::select("select * from employees");
        //  return view("all_employee",["employees"=>$employees]);
         try {           
            //DB::connection()->getPdo();            
          $employees=DB::select("select * from employees");        
          return view("all_employee",["employees"=>$employees]);
  
          }catch (\Exception $e) {
              die("Error: ".$e->getMessage());
          }
         
    }


}
